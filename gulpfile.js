// gulp modules
var gulp = require('gulp');
var less = require('gulp-less');
var pleeease = require('gulp-pleeease');
var shell = require('gulp-shell');
var sourcemaps = require('gulp-sourcemaps');
var taskListing = require('gulp-task-listing');

var isProduction = function() {
  return process.env.ENV === 'production';
};

gulp.task('vendor', function() {
  gulp.src('jasminetea/resources/vendor/**')
    .pipe(gulp.dest('jasminetea/static/vendor'));
});

gulp.task('less', function() {
  gulp.src('jasminetea/resources/styles/less/*.less')
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('jasminetea/static/styles'));
});

gulp.task('javascript', function() {
  gulp.src('jasminetea/resources/**/*.js')
    .pipe(gulp.dest('jasminetea/static/'));
});

gulp.task('image', function() {
  gulp.src('jasminetea/resources/images/**')
    .pipe(gulp.dest('jasminetea/static/images'));
});

gulp.task('watch', function() {
  gulp.watch('jasminetea/resources/**/*.styl', ['stylus']);
  gulp.watch('jasminetea/resources/**/*.less', ['less']);
  gulp.watch('jasminetea/resources/**/*.js', ['javascript']);
});

gulp.task('deploy', ['vendor', 'stylus', 'javascript', 'image', 'db:migrate']);

gulp.task('default', taskListing.withFilters(null, 'default'));

gulp.task('tt', function() {
  console.log(isProduction())
}); 

