from jasminetea.models import DBSession, User
import factory
import faker
from factory.alchemy import SQLAlchemyModelFactory

fake = faker.Factory.create()


class UserFactory(SQLAlchemyModelFactory):
    class Meta:
        model = User
        sqlalchemy_session = DBSession   # the SQLAlchemy session object

    username = factory.LazyAttribute(lambda x: fake.user_name())
    password = 'Password123'
    email = factory.Sequence(lambda x: fake.email())
