"""Create user

Revision ID: a8c5fd0f99
Revises: None
Create Date: 2014-11-08 12:52:02.506897

"""

# revision identifiers, used by Alembic.
revision = 'a8c5fd0f99'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('email', sa.String(), nullable=True),
    sa.Column('username', sa.String(), nullable=True),
    sa.Column('password', sa.String(), nullable=True),
    sa.Column('last_logged', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('users_index_email', 'user', ['email'], unique=True)
    op.create_index('users_index_username', 'user', ['username'], unique=True)
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('users_index_username', table_name='user')
    op.drop_index('users_index_email', table_name='user')
    op.drop_table('user')
    ### end Alembic commands ###
