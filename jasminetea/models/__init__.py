from datetime import datetime

from sqlalchemy import (Column, Index, Integer, String, DateTime)
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import scoped_session, sessionmaker
from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))


class Base:

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = Column(Integer, primary_key=True)
    name = Column(String)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.utcnow,
                        onupdate=datetime.utcnow)

    _repr_hide = ['created_at', 'updated_at']

    @classmethod
    def query(cls):
        return DBSession.query(cls)

    @classmethod
    def get(cls, id):
        return cls.query().get(id)

    @classmethod
    def get_by(cls, **kw):
        return cls.query().filter_by(**kw).first()

    @classmethod
    def get_or_create(cls, **kw):
        r = cls.get_by(**kw)
        if not r:
            r = cls(**kw)
            DBSession.add(r)
        return r


Base = declarative_base(cls=Base)


class User(Base):
    email = Column(String)
    username = Column(String)
    password = Column(String)
    last_logged = Column(DateTime, default=datetime.utcnow)


    @classmethod
    def first(cls):
        return cls.query().first()


Index('users_index_username', User.username, unique=True)
Index('users_index_email', User.email, unique=True)
