import transaction
from .factories import UserFactory


def create_sample_data():
    with transaction.manager:
        UserFactory.create()
