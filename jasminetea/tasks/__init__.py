from invoke import Collection, task
from . import db


@task(default=True)
def default():
    print("'jasminetea --list' to see all tasks")


namespace = Collection(db, default)
