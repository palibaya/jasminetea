from invoke import task
from alembic import command as alembic_cmd
from jasminetea import utils
from ..models import Base
from ..seeds import create_sample_data

config = utils.get_alembic_config()
utils.bind_db_engine()


@task()
def revision(message, autogenerate=True):
    alembic_cmd.revision(config, message, autogenerate)

@task()
def upgrade(revision='head', sql=False, tag=None):
    alembic_cmd.upgrade(config, revision, tag)

@task()
def downgrade(revision, sql=False, tag=None):
    alembic_cmd.downgrade(config, revision, tag)

@task()
def history(rev_range=None):
    alembic_cmd.history(config, rev_range)

@task()
def current(head_only=False):
    alembic_cmd.current(config, head_only)

@task()
def stamp(revision, sql=False, tag=None):
    alembic_cmd.stamp(config, revision=revision)

@task()
def seeds():
    create_sample_data()

@task()
def fix():
    engine = utils.get_db_engine()
    Base.metadata.drop_all(engine)
    engine.execute('drop table alembic_version')
    alembic_cmd.upgrade(config, 'head')
