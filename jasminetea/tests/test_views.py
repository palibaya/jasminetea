import unittest
import transaction

from pyramid import testing

from jasminetea.models import DBSession, Base
from jasminetea.factories import UserFactory
from jasminetea import views


class TestMyViewSuccessCondition(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        from sqlalchemy import create_engine
        engine = create_engine('sqlite://')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            UserFactory.create(username='first')

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_passing_view(self):
        request = testing.DummyRequest()
        info = views.my_view(request)
        self.assertEqual(info['user'].username, 'first')
        self.assertEqual(info['project'], 'jasminetea')


class TestMyViewFailureCondition(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        from sqlalchemy import create_engine
        engine = create_engine('sqlite://')
        DBSession.configure(bind=engine)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_failing_view(self):
        request = testing.DummyRequest()
        info = views.my_view(request)
        self.assertEqual(info.status_int, 500)
