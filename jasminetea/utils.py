# This module just use for development

import os
from pyramid.paster import get_appsettings as appsettings
from sqlalchemy import engine_from_config
from alembic.config import Config as AlembicConfig
from .models import DBSession

from .models import DBSession

def get_file_config():
    return "%s.ini" % os.environ.get('ENV', 'development')

def get_alembic_config():
    return AlembicConfig(get_file_config())

def get_appsettings():
    return appsettings(get_file_config())

def get_db_engine():
    return engine_from_config(get_appsettings(), 'sqlalchemy.')

def bind_db_engine():
    DBSession.configure(bind=get_db_engine())
