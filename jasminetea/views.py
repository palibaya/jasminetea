from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError

from .models import User


# @view_config(route_name='home', renderer='templates/mytemplate.genshi')
def my_view(request):
    try:
        user = User.first()
    except DBAPIError:
        return Response('Error', content_type='text/plain', status_int=500)
    return {'user': user, 'project': 'jasminetea'}


@view_config(route_name='home', renderer='templates/base.genshi')
def home(request):
    return {}
