import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_genshi',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'SQLAlchemy',
    'alembic',
    'factory_boy',
    'fake-factory',
    'psycopg2',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    'bpython',
    'invoke',
    'nose2',
    ]

setup(
    name='jasminetea',
    version='0.0',
    description='jasminetea',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    author='',
    author_email='',
    url='',
    keywords='web wsgi bfg pylons pyramid',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='jasminetea',
    install_requires=requires,
    entry_points={
        'paste.app_factory': ['main = jasminetea:main'],
        'console_scripts': [
            'jasminetea = invoke.cli:main'
        ]
    }
)
